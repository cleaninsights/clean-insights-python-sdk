import json
from datetime import datetime, timedelta, timezone
from os import PathLike
from pathlib import Path
from typing import Any, Callable, Dict
from typing import List
from typing import Optional
from typing import TypeVar
from typing import Union

from cleaninsights.campaign import Campaign
from cleaninsights.ci_json import CleanInsightsEncoder
from cleaninsights.conf import Configuration
from cleaninsights.consent_request_ui import ConsentRequestUi
from cleaninsights.consents import CampaignConsent, Consent, ConsentState, Feature, FeatureConsent
from cleaninsights.datapoint import DataPoint
from cleaninsights.event import Event
from cleaninsights.insights import Insights
from cleaninsights.store import Store
from cleaninsights.store.default import DefaultStore
from cleaninsights.visit import Visit

D = TypeVar('D', DataPoint, Event, Visit)


class CleanInsights:
    """Core class for the CleanInsights SDK."""

    __auto_track_ci: Optional['CleanInsights'] = None

    @classmethod
    def auto_track(cls, storage_dir: Path, server: str, site_id: int,
                   campaigns: Optional[Dict[str, Campaign]] = None) -> 'CleanInsights':
        """
        Instantiates a singleton `CleanInsights` object with a default configuration
        and a default campaign named "visits" which never expires.

        The "visits" campaign will have an `aggregation_period_length` of 1 day.
        That means, starts will be collected during 24 hours and then sent to the server,
        evenly distributed over that day, in order to keep user privacy.

        After instantiation, it will automatically track a start of the app, but ONLY, if consent is given.

        You will need to show some consent form to the user, so the user
        is able to agree to this tracking before setting consent.

        Refer to the example code to see how consent is handled.

        :param storage_dir: The directory the store will be located in.

        :param server: The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.

        :param site_id: The Matomo site ID to record page visits for.

        :param campaigns: (Additional) campaign configurations.

        :return: The instantiated `CleanInsights` object.
        """
        if cls.__auto_track_ci:
            return cls.__auto_track_ci

        campaigns = campaigns or {}

        if "visits" not in campaigns:
            campaigns["visits"] = Campaign(
                datetime(1970, 1, 1, tzinfo=timezone.utc),
                datetime(2099, 12, 31, 23, 59, 59, tzinfo=timezone.utc),
                1, 50000)

        conf = Configuration(server, site_id, campaigns, persist_every_n_times=1, debug=True)

        ci = CleanInsights(conf, DefaultStore(storage_dir=storage_dir))
        cls.__auto_track_ci = ci

        ci.measure_visit([], "visits")

        return ci

    def __init__(self, conf: Union[Configuration, PathLike, str], store: Store):
        if isinstance(conf, Configuration):
            self.conf = conf
        elif isinstance(conf, PathLike):
            with open(conf, 'r') as file:
                data = json.load(file)
            self.conf = Configuration.from_dict(data)
        elif isinstance(conf, str):
            data = json.loads(conf)
            self.conf = Configuration.from_dict(data)

        self._store = store
        self._persistence_counter = 0
        self._sending = False
        self._failed_submission_count = 0
        self._last_failed_submission = datetime.fromtimestamp(0)

    def measure_visit(self, scene_path: List[str], campaign_id: str, timestamp: Optional[datetime] = None):
        """
        Track a scene visit.

        :param scene_path: A hierarchical path best describing the structure of your scenes.
            E.g. `['Main', 'Settings', 'Some Setting']`.

        :param campaign_id: The campaign ID as per your configuration, where this measurement belongs to.

        :param timestamp: Optional timestamp when the visit happened for use in retrograde log analysis and such.
            Ignored, when `Configuration.server_side_anonymous_usage` is `False`!
        """
        campaign = self._get_campaign_if_good(campaign_id, "/".join(scene_path))

        if timestamp and not self.conf.server_side_anonymous_usage:
            timestamp = None

        if campaign:
            where: Callable[[Visit], bool] = lambda v: "/".join(v.scene_path) == "/".join(scene_path)

            visit = self._get_and_measure(self._store.visits, campaign_id, campaign, where, timestamp)

            if visit:
                self._debug("Gain visit insight: {0}", visit)
            else:
                # Align first and last timestamps with campaign measurement period,
                # in order not to accidentally leak more information than promised.
                period = campaign.current_measurement_period if timestamp is None else campaign.aggregation_period(
                    timestamp)

                if period:
                    visit = Visit(scene_path, campaign_id, 1, period.start, period.end)
                    self._store.visits.append(visit)

                    self._debug("Gain visit insight: {0}", visit)
                else:
                    if timestamp is None:
                        self._debug("campaign.current_measurement_period == None! This should not happen!")

        self._persist_and_send()

    def measure_event(self,
                      category: str,
                      action: str,
                      campaign_id: str,
                      name: Optional[str] = None,
                      value: Optional[Union[int, float]] = None):
        """
        Track an event.

        :param category: The event category. Must not be empty. (e.g. Videos, Music, Games...)

        :param action: The event action. Must not be empty.
            (e.g. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)

        :param campaign_id: The campaign ID as per your configuration, where this measurement belongs to.

        :param name: The event name.

        :param value: The event value.
        """
        campaign = self._get_campaign_if_good(campaign_id, f"{category}/{action}")

        if campaign:
            where: Callable[[Event], bool] = lambda e: e.category == category and e.action == action and e.name == name

            event = self._get_and_measure(self._store.events, campaign_id, campaign, where)

            if event:
                campaign.apply(value, event)

                self._debug("Gain event insight: {0}", event)
            else:
                # Align first and last timestamps with campaign measurement period,
                # in order not to accidentally leak more information than promised.
                period = campaign.current_measurement_period

                if period:
                    event = Event(category, action, name, value, campaign_id, 1, period.start, period.end)
                    self._store.events.append(event)

                    self._debug("Gain event insight: {0}", event)
                else:
                    self._debug("campaign.current_measurement_period == None! This should not happen!")

        self._persist_and_send()

    @property
    def feature_consents(self) -> List[str]:
        return list(self._store.consents.features.keys())

    @property
    def campaign_consents(self) -> List[str]:
        return list(self._store.consents.campaigns.keys())

    def feature_consent_by_index(self, index: int) -> Optional[FeatureConsent]:
        if index < 0:
            return None

        keys = self._store.consents.features.keys()

        if index >= len(keys):
            return None

        feature = list(keys)[index]

        return self._store.consents.consent_for_feature(Feature(feature))

    def campaign_consent_by_index(self, index: int) -> Optional[CampaignConsent]:
        if index < 0:
            return None

        keys = self._store.consents.campaigns.keys()

        if index >= len(keys):
            return None

        campaign_id = list(keys)[index]

        return self._store.consents.consent_for_campaign(campaign_id)

    def grant_feature(self, feature):
        consent = self._store.consents.grant_feature(feature)

        self._persist_and_send()

        return consent

    def deny_feature(self, feature):
        consent = self._store.consents.deny_feature(feature)

        self._persist_and_send()

        return consent

    def consent_for_feature(self, feature: Feature) -> Optional[FeatureConsent]:
        """
        Returns the consent for a given feature, if any available.

        :param feature: The feature to get the consent for.
        :return: The `FeatureConsent` for the given feature or `None`, if consent unknown.
        """
        if self.conf.server_side_anonymous_usage:
            return FeatureConsent(feature, Consent(False))

        return self._store.consents.consent_for_feature(feature)

    def state_of_feature(self, feature: Feature) -> ConsentState:
        """
        Checks the consent state of a feature.

        :param feature: The feature to check the consent state of.
        :return: The current state of consent.
        """
        if self.conf.server_side_anonymous_usage:
            return ConsentState.DENIED

        return self._store.consents.state_of_feature(feature)

    def grant_campaign(self, campaign_id: str) -> Optional[CampaignConsent]:
        if campaign_id not in self.conf.campaigns:
            return None

        consent = self._store.consents.grant_campaign(campaign_id, self.conf.campaigns[campaign_id])

        self._persist_and_send()

        return consent

    def deny_campaign(self, campaign_id: str):
        if campaign_id not in self.conf.campaigns:
            return None

        consent = self._store.consents.deny_campaign(campaign_id)

        self._persist_and_send()

        return consent

    def is_campaign_currently_granted(self, campaign_id: str) -> bool:
        return self.state_of_campaign(campaign_id) == ConsentState.GRANTED

    def consent_for_campaign(self, campaign_id: str) -> Optional[CampaignConsent]:
        """
        Returns the consent for a given campaign, if any available.

        :param campaign_id: The campaign ID to get the consent for.
        :return: The `CampaignConsent` for the given campaign or `None`, if consent unknown.
        """
        if self.conf.server_side_anonymous_usage:
            return CampaignConsent(campaign_id, Consent(True))

        if campaign_id not in self.conf.campaigns:
            return None

        campaign = self.conf.campaigns[campaign_id]

        now = datetime.now(campaign.end.tzinfo)

        if now >= campaign.end:
            return None

        return self._store.consents.consent_for_campaign(campaign_id)

    def state_of_campaign(self, campaign_id: str) -> ConsentState:
        """
        Checks the consent state of a campaign.

        :param campaign_id: The campaign ID to check the consent state of.
        :return: The current state of consent.
        """
        if self.conf.server_side_anonymous_usage:
            return ConsentState.GRANTED

        if campaign_id not in self.conf.campaigns:
            return ConsentState.UNCONFIGURED

        campaign = self.conf.campaigns[campaign_id]

        now = datetime.now(campaign.end.tzinfo)

        if now >= campaign.end:
            return ConsentState.UNCONFIGURED

        return self._store.consents.state_of_campaign(campaign_id)

    def request_consent_for_campaign(self, campaign_id: str, consent_request_ui: ConsentRequestUi,
                                     completed: Optional[ConsentRequestUi.CompletedCampaign] = None):

        campaign = self.conf.campaigns.get(campaign_id, None)

        if campaign is None:
            self._debug("Cannot request consent: Campaign '{}' not configured.", campaign_id)

            if completed is not None:
                completed(None)

            return

        now = datetime.now(campaign.end.tzinfo)

        if now > campaign.end:
            self._debug("Cannot request consent: End of campaign '{}' reached.", campaign_id)

            if completed is not None:
                completed(None)

            return

        if campaign.next_total_measurement_period() is None:
            self._debug(
                "Cannot request consent: Campaign '{}' configuration seems messed up.", campaign_id)

            if completed is not None:
                completed(None)

            return

        consent = self._store.consents.consent_for_campaign(campaign_id)

        if consent:
            self._debug(
                "Already asked for consent for campaign '{0}'. It was {1}", campaign_id,
                f"granted between {consent.start} and {consent.end}" if consent.granted else f"denied on {consent.start}")

            if completed is not None:
                completed(None)

            return

        def complete(granted: bool, length: Optional[int]):
            consent: CampaignConsent

            if granted:
                consent = self._store.consents.grant_campaign(campaign_id, campaign, length)
            else:
                consent = self._store.consents.deny_campaign(campaign_id)

            if completed is not None:
                completed(consent)

        consent_request_ui.show_campaign(campaign_id, campaign, complete)

    def request_consent_for_feature(
            self, feature: Feature,
            consent_request_ui: ConsentRequestUi, completed: Optional[ConsentRequestUi.CompletedFeature] = None):

        consent = self._store.consents.consent_for_feature(feature)

        if consent:
            self._debug("Already asked for consent for feature '{0}'. It was {1} on {2}.", feature,
                        "granted" if consent.granted else "denied", consent.start)

            if completed is not None:
                completed(None)

            return

        def complete(granted: bool):
            consent: FeatureConsent

            if granted:
                consent = self._store.consents.grant_feature(feature)
            else:
                consent = self._store.consents.deny_feature(feature)

            if completed is not None:
                completed(consent)

        consent_request_ui.show_feature(feature, complete)

    def test_server(self, done: Callable[[Union[Exception, None]], None]):
        """
        Sends an empty body to the server for easy debugging of server-related issues like TLS and CORS problems.

        **DON'T LEAVE THIS IN PRODUCTION**, once you're done fixing any server issues. There's absolutely no point in
        pinging the server with this all the time, it will undermine your privacy promise to your users!

        :param done: Callback, when the operation is finished, either successfully or not.
        """
        def done_inner(error: Optional[Exception]):
            if error is None:
                error = Exception(
                    'Server replied with no error while it should have responded with HTTP 400 Bad Request!')

            # Success
            if error.__str__().startswith('HTTP Error 400:'):
                done(None)
                return

            done(error)

        self._store.send("", self.conf.server, self.conf.timeout, done_inner)

    def persist(self):
        """
        Persist accumulated data to the filesystem.

        An app should call this when the process exits for whatever reason.
        """
        self._persist(True, False)

    def _persist(self, asynchronous: bool, force: bool = False):
        """
        Persist accumulated data to the filesystem.

        :param asynchronous: If true, returns immediately and does persistence asynchronously, only if it's already due.
        :param force: Write regardless of threshold reached.
        """
        self._persistence_counter += 1

        if force or self._persistence_counter >= self.conf.persist_every_n_times:
            def done(error: Optional[Exception]):
                if error:
                    self._debug(error.__str__())
                else:
                    self._persistence_counter = 0

                    self._debug("Data persisted to storage.")

            self._store.persist(asynchronous, done)

    def _persist_and_send(self):
        self._persist(True)

        if self._sending:
            self._debug("Data sending already in progress.")
            return

        self._sending = True

        if self._failed_submission_count > 0:
            # Calculate a delay for the next retry:
            # Minimum is 2 times the configured network timeout after the first failure,
            # exponentially increasing with number of retries.
            # Maximum is every conf.max_retry_delay interval.
            exp = self._last_failed_submission + timedelta(
                seconds=self.conf.timeout * (2 ** self._failed_submission_count))
            tru = self._last_failed_submission + timedelta(seconds=self.conf.max_retry_delay)

            now = datetime.now(timezone.utc)

            if now < min(exp, tru):
                self._sending = False
                self._debug("Waiting longer to send data after {0} failed attempts.", self._failed_submission_count)
                return

        insights = Insights(self.conf, self._store)

        if insights.is_empty:
            self._sending = False
            self._debug("No data to send.")
            return

        def done(error: Optional[Exception]):
            if error:
                self._last_failed_submission = datetime.now(timezone.utc)
                self._failed_submission_count += 1
                self._debug(error.__str__())
            else:
                self._last_failed_submission = datetime.fromtimestamp(0)
                self._failed_submission_count = 0

                insights.clean(self._store)

                self._debug("Successfully sent data.")

                self._persist(True, True)

            self._sending = False

        data = json.dumps(insights, cls=CleanInsightsEncoder)

        self._store.send(data, self.conf.server, self.conf.timeout, done)

    def _get_campaign_if_good(self, campaign_id: str, debug_str: str) -> Optional[Campaign]:
        campaign = self.conf.campaigns.get(campaign_id, None)

        if campaign is None:
            self._debug("Measurement '{0}' discarded, because campaign '{1}' is missing in configuration.",
                        debug_str, campaign_id)
            return None

        now = datetime.now(campaign.end.tzinfo)

        if now < campaign.start:
            self._debug("Measurement '{0}' discarded, because campaign '{1}' didn't start, yet.",
                        debug_str, campaign_id)
            return None

        if now > campaign.end:
            self._debug("Measurement '{0}' discarded, because campaign '{1}' already ended.",
                        debug_str, campaign_id)
            return None

        if not self.is_campaign_currently_granted(campaign_id):
            self._debug(
                "Measurement '{0}' discarded, because campaign '{1}' has no user consent yet, any more ore we're outside the measurement period.",
                debug_str, campaign_id)
            return None

        return campaign

    def _get_and_measure(self, haystack: List[D], campaign_id: str, campaign: Campaign, where: Callable[[D], bool],
                         timestamp: Optional[datetime] = None) -> Optional[D]:
        """
        Get a `DataPoint` subclass out of the `haystack`, as long is it fits the `campaign`.
        Increases `times` according to the campaign rules.

        Create a new `DataPoint` of nothing is returned here.

        :param haystack: The haystack full of `DataPoint` subclasses.
        :param campaign_id: The campaign ID it must match.
        :param campaign: The campaign parameters to match against.
        :param where: Additional condition for selection.
        :return: A `DataPoint` subclass out of the `haystack`, as long as it fits the `campaign`.
        """
        period = campaign.current_measurement_period if timestamp is None else campaign.aggregation_period(timestamp)

        if period is None:
            if timestamp is None:
                self._debug("campaign.current_measurement_period == None! This should not happen!")

            return None

        def is_needle(d: D):
            return (d.campaign_id == campaign_id
                    and period.start <= d.first <= period.end
                    and period.start <= d.last <= period.end
                    and where(d))

        datapoint = next(iter([d for d in haystack if is_needle(d)]), None)

        if datapoint is None:
            return None

        if not campaign.only_record_once:
            datapoint.times += 1

        return datapoint

    def _debug(self, format: str, *args: Optional[Any]):
        CleanInsights._dbg(self.conf.debug, format, *args)

    @classmethod
    def _dbg(cls, toggle: bool, format: str, *args: Optional[Any]):
        if not toggle:
            return

        print(format.format(*args))
