from datetime import datetime
from typing import NamedTuple


class AggregationPeriod(NamedTuple):
    """An aggregation period with a defined start and end date."""

    start: datetime
    """The start date."""

    end: datetime
    """The end date."""
