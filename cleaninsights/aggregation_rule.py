from enum import Enum


class EventAggregationRule(Enum):
    """The rule how to aggregate the value of an event (if any given) with subsequent calls."""

    SUM = "sum"
    """Calculate the sum of the values."""

    AVG = "avg"
    """Calculate the mean average of the values."""
