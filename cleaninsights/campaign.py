from dataclasses import dataclass
from datetime import datetime
from datetime import timedelta
from typing import Any
from typing import Dict
from typing import Optional

from cleaninsights.aggregation_period import AggregationPeriod
from cleaninsights.aggregation_rule import EventAggregationRule
from cleaninsights.event import Event


@dataclass
class Campaign:
    """
    A logical grouping for event and visit measurements.
    This grouping is user-defined and can be bounded by time and with specific
    measurement properties.
    """

    def __init__(self, start: datetime, end: datetime, aggregation_period_length: int, number_of_periods: int = 1,
                 only_record_once: bool = False,
                 event_aggregation_rule: EventAggregationRule = EventAggregationRule.SUM,
                 strengthen_anonymity: bool = False):
        """
        :param start: The start date for this campaign. (inclusive)
            Measurements to be recorded before this date will be ignored.

        :param end: The end date for this campaign. (inclusive)
            Measurements to be recorded before this date will be ignored.

        :param aggregation_period_length: The length of the aggregation period in number of days.
            At the end of a period, the aggregated data will be sent to the analytics server.

        :param number_of_periods: The number of periods you want to measure in a row.
            Therefore, the total length in days you measure one user is
            `aggregation_period_length * number_of_periods` beginning with the first
            day of the next period after the user consented.

        :param only_record_once: Will result in recording only the first time a visit or event happened per period.
            Useful for yes/no questions.

        :param event_aggregation_rule: The rule how to aggregate the value of an event (if any given)
            with subsequent calls.

        :param strengthen_anonymity: When set to true, measurements only ever start at the next full period.
            This ensures, that anonymity guaranties aren't accidentally reduced because the
            first period is very short.
        """
        self.start = start
        self.end = end
        self.aggregation_period_length = aggregation_period_length
        self.number_of_periods = number_of_periods
        self.only_record_once = only_record_once
        self.event_aggregation_rule = event_aggregation_rule
        self.strengthen_anonymity = strengthen_anonymity

    @property
    def current_measurement_period(
            self) -> Optional[AggregationPeriod]:  # noqa: D401
        """
        Alias for :attr:`Campaign.current_aggregation_period`.
        **Deprecated.** This name is used in the TypeScript and iOS SDKs,
        however is not consistent with usage in the Campaign object.
        """
        return self.current_aggregation_period

    @property
    def current_aggregation_period(
            self) -> Optional[AggregationPeriod]:  # noqa: D401
        """
        The current aggregation period for this campaign.
        The period defines start (inclusive) and end (exclusive) dates. If it
        is currently before or after the campaign's :attr:`start
        <Campaign.start>` or :attr:`end <Campaign.end>` dates, `None` will be
        returned.
        """
        return self.aggregation_period(datetime.now(self.start.tzinfo))

    def aggregation_period(
            self, dt: datetime) -> Optional[AggregationPeriod]:  # noqa: D401
        """
        The aggregation period for this campaign in which `dt` falls.
        The period defines start (inclusive) and end (exclusive) dates. If it
        is currently before or after the campaign's :attr:`start
        <Campaign.start>` or :attr:`end <Campaign.end>` dates, `None` will be
        returned.
        """
        if self.number_of_periods <= 0:
            return None

        period_end = self.start

        while True:
            period_end += timedelta(days=self.aggregation_period_length)

            if period_end > dt:
                break

        period_start = period_end - timedelta(days=self.aggregation_period_length)

        if period_start < self.start:
            period_start = self.start

        if period_end > self.end:
            period_end = self.end

        if period_start > dt or period_end < dt:
            return None

        return AggregationPeriod(period_start, period_end)

    def next_total_measurement_period(self) -> Optional[AggregationPeriod]:
        current = self.current_measurement_period

        if current is None:
            return None

        period_start = current.end if self.strengthen_anonymity else current.start
        period_end = period_start

        counter = 0

        while counter < self.number_of_periods and period_end + timedelta(days=self.aggregation_period_length) <= self.end:
            period_end += timedelta(days=self.aggregation_period_length)
            counter += 1

        if period_start == period_end:
            return None

        return AggregationPeriod(period_start, period_end)

    def apply(self, value: Optional[float], event: Event):
        """
        Apply the `event_aggregation_rule` to the given event with the given value.

        :param value: The value to apply.
        :param event: The event to apply the value to.
        """
        if value is None or self.only_record_once:
            return

        old_val = 0 if event.value is None else event.value

        if self.event_aggregation_rule == EventAggregationRule.SUM:
            event.value = old_val + value
        elif self.event_aggregation_rule == EventAggregationRule.AVG:
            event.value = (old_val * (event.times - 1) + value) / event.times

    @classmethod
    def from_dict(cls, data: Dict[str, Any]):
        """
        Configuration from a dictionary, potentially from a JSON according to the specification
        found at https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/configuration.schema.json?ref_type=heads
        resp. https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/configuration-properties-measurement-campaigns-patternproperties-unique-campaign-id.md
        """
        if "start" not in data or not data["start"]:
            raise ValueError("start")
        start = data["start"]
        if isinstance(start, str):
            start = datetime.fromisoformat(start)
        if not isinstance(start, datetime):
            raise TypeError("start")

        if "end" not in data or not data["end"]:
            raise ValueError("end")
        end = data["end"]
        if isinstance(end, str):
            end = datetime.fromisoformat(end)
        if not isinstance(end, datetime):
            raise TypeError("end")

        aggregation_period_length = 0
        if "aggregationPeriodLength" in data:
            aggregation_period_length = data["aggregationPeriodLength"]
        elif "aggregation_period_length" in data:
            aggregation_period_length = data["aggregation_period_length"]

        if not isinstance(aggregation_period_length, int):
            raise TypeError("aggregation_period_length")
        if aggregation_period_length < 1:
            raise ValueError("aggregation_period_length")

        number_of_periods = 1
        if "numberOfPeriods" in data:
            number_of_periods = data["numberOfPeriods"]
        elif "number_of_periods" in data:
            number_of_periods = data["number_of_periods"]

        if not isinstance(number_of_periods, int) or number_of_periods < 1:
            number_of_periods = 1

        only_record_once = False
        if "onlyRecordOnce" in data:
            only_record_once = data["onlyRecordOnce"]
        elif "only_record_once" in data:
            only_record_once = data["only_record_once"]

        if not isinstance(only_record_once, bool):
            only_record_once = False

        event_aggregation_rule = EventAggregationRule.SUM
        if "eventAggregationRule" in data:
            event_aggregation_rule = data["eventAggregationRule"]
        elif "event_aggregation_rule" in data:
            event_aggregation_rule = data["event_aggregation_rule"]

        if isinstance(event_aggregation_rule, str):
            event_aggregation_rule = EventAggregationRule(event_aggregation_rule)
        if not isinstance(event_aggregation_rule, EventAggregationRule) or not event_aggregation_rule:
            event_aggregation_rule = EventAggregationRule.SUM

        strengthen_anonymity = False
        if "strengthenAnonymity" in data:
            strengthen_anonymity = data["strengthenAnonymity"]
        elif "strengthen_anonymity" in data:
            strengthen_anonymity = data["strengthen_anonymity"]

        if not isinstance(strengthen_anonymity, bool):
            strengthen_anonymity = False

        return Campaign(start,
                        end,
                        aggregation_period_length,
                        number_of_periods,
                        only_record_once,
                        event_aggregation_rule,
                        strengthen_anonymity)
