from datetime import datetime, timezone
from json import JSONDecoder, JSONEncoder

from cleaninsights.consents import Consent, Consents
from cleaninsights.event import Event
from cleaninsights.insights import Insights
from cleaninsights.store import Store
from cleaninsights.visit import Visit


class CleanInsightsEncoder(JSONEncoder):

    def default(self, obj):
        if isinstance(obj, Store):
            return {
                "consents": obj.consents,
                "visits": obj.visits,
                "events": obj.events
            }

        elif isinstance(obj, Consents):
            return {
                "features": obj.features,
                "campaigns": obj.campaigns
            }

        elif isinstance(obj, Consent):
            return {
                "granted": obj.granted,
                "start": obj.start,
                "end": obj.end
            }

        elif isinstance(obj, datetime):
            return round(obj.timestamp())

        elif isinstance(obj, Visit):
            return {
                "action_name": '/'.join(obj.scene_path),
                "campaign_id": obj.campaign_id,
                "times": obj.times,
                "period_start": obj.first,
                "period_end": obj.last
            }

        elif isinstance(obj, Event):
            return {
                "category": obj.category,
                "action": obj.action,
                "name": obj.name,
                "value": obj.value,
                "campaign_id": obj.campaign_id,
                "times": obj.times,
                "period_start": obj.first,
                "period_end": obj.last,
            }

        elif isinstance(obj, Insights):
            return {
                "idsite": obj.idsite,
                "lang": obj.lang,
                "ua": obj.ua,
                "visits": obj.visits,
                "events": obj.events
            }

        else:
            return super().default(obj)


class CleanInsightsDecoder(JSONDecoder):

    def __init__(self, *args, **kwargs):
        kwargs["object_hook"] = self.object_hook

        super().__init__(*args, **kwargs)

    def object_hook(self, dct):
        if "features" in dct and "campaigns" in dct:
            return Consents(dct["features"], dct["campaigns"])

        elif ("action_name" in dct and "campaign_id" in dct and "times" in dct and "period_start" in dct
              and "period_end" in dct):
            scene_path = dct["action_name"].split("/")

            if len(scene_path) == 1 and scene_path[0] == "":
                scene_path = []

            return Visit(scene_path, dct["campaign_id"], dct["times"],
                         datetime.fromtimestamp(dct["period_start"], timezone.utc),
                         datetime.fromtimestamp(dct["period_end"], timezone.utc))

        elif ("category" in dct and "action" in dct and "name" in dct and "value" in dct and "campaign_id" in dct
              and "times" in dct and "period_start" in dct and "period_end" in dct):
            return Event(dct["category"], dct["action"], dct["name"], dct["value"], dct["campaign_id"], dct["times"],
                         datetime.fromtimestamp(dct["period_start"], timezone.utc),
                         datetime.fromtimestamp(dct["period_end"], timezone.utc))

        else:
            return dct
