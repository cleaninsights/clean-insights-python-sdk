from dataclasses import dataclass
from typing import Any, Callable
from typing import Dict

from cleaninsights.campaign import Campaign


@dataclass
class Configuration:

    def __init__(self, server: str, site_id: int, campaigns: Dict[str, Campaign], timeout: int = 5,
                 max_retry_delay: int = 3600,
                 max_age_of_old_data: int = 100, persist_every_n_times: int = 10,
                 server_side_anonymous_usage: bool = False, debug: bool = False):
        """
        :param server: The server URL should look like `https://myhost.example.com/ci/cleaninsights.php`.

        :param site_id: The Matomo site ID to record this data for.

        :param campaigns: Campaign configuration.

        :param timeout: Connection timeout in seconds.

        :param max_retry_delay: The SDK uses a truncated exponential backoff strategy on server failures.
            So the delay until it retries will rise exponentially, until it reaches `max_retry_delay` seconds.

        :param max_age_of_old_data: The number in days of how long the SDK will try to keep sending old measurements.
            If the measurements become older than that, they will be purged.

        :param persist_every_n_times: Regulates, how often data persistence is done.
            If set to 1, every time something is tracked, *ALL* data is stored to disk.
            The more you track, the higher you should set this to avoid heavy load due
            to disk I/O.

        :param server_side_anonymous_usage: When set to `True`, assumes consent for all campaigns and none for features.
            Only use this, when you're running on the server and don't measure anything users
            might need to give consent to!

        :param debug: When set, CleanInsights SDK will print some debug output to STDOUT.
        """
        self.server = server
        self.site_id = site_id
        self.campaigns = campaigns
        self.timeout = timeout
        self.max_retry_delay = max_retry_delay
        self.max_age_of_old_data = max_age_of_old_data
        self.persist_every_n_times = persist_every_n_times
        self.server_side_anonymous_usage = server_side_anonymous_usage
        self.debug = debug

    def check(self, debug: Callable[[str], None]) -> bool:
        """
        Checks configuration for some well-known problems, emits a debug message and returns false, if one found.

        :param debug: Function to handle the debug message.
        :return: `True`, if config seems ok, `False` if known problems exist.
        """
        if not self.server or not self.server.startswith("http"):
            debug("Configuration problem: 'server' is not defined properly. It needs to be a full URL like this: "
                  "'https://example.org/cleaninsights.php'!")

            return False

        if not self.site_id or self.site_id < 1:
            debug("Configuration problem: 'siteId' is not defined properly. It needs to be a positive integer value!")

            return False

        if len(self.campaigns) < 1:
            debug("Configuration problem: No campaign defined!")

            return False

        return True

    @classmethod
    def from_dict(cls, data: Dict[str, Any]):
        """
        Configuration from a dictionary, potentially from a JSON according to the specification
        found at https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schemas/configuration.schema.json?ref_type=heads
        resp. https://gitlab.com/cleaninsights/clean-insights-design/-/blob/master/schema-docs/configuration.md
        """
        server = data["server"]
        if not isinstance(server, str):
            raise TypeError("server")
        if not server:
            raise ValueError("server")

        site_id = data["siteId"] if "siteId" in data else data["site_id"]
        if not isinstance(site_id, int):
            raise TypeError("site_id")
        if site_id < 1:
            raise ValueError("site_id")

        timeout = data["timeout"] if "timeout" in data else 5
        if not (isinstance(timeout, float) or isinstance(timeout, int)) or timeout < 0.1:
            timeout = 5

        max_retry_delay = 3600
        if "maxRetryDelay" in data:
            max_retry_delay = data["maxRetryDelay"]
        elif "max_retry_delay" in data:
            max_retry_delay = data["max_retry_delay"]

        if not (isinstance(max_retry_delay, float) or isinstance(max_retry_delay, int)) or max_retry_delay < 0.1:
            max_retry_delay = 3600

        max_age_of_old_data = 100
        if "maxAgeOfOldData" in data:
            max_age_of_old_data = data["maxAgeOfOldData"]
        elif "max_age_of_old_data" in data:
            max_age_of_old_data = data["max_age_of_old_data"]

        if not isinstance(max_age_of_old_data, int) or max_age_of_old_data < 1:
            max_age_of_old_data = 100

        persist_every_n_times = 10
        if "persistEveryNTimes" in data:
            persist_every_n_times = data["persistEveryNTimes"]
        elif "persist_every_n_times" in data:
            persist_every_n_times = data["persist_every_n_times"]

        if not isinstance(persist_every_n_times, int) or persist_every_n_times < 1:
            persist_every_n_times = 10

        server_side_anonymous_usage = False
        if "serverSideAnonymousUsage" in data:
            server_side_anonymous_usage = data["serverSideAnonymousUsage"]
        elif "server_side_anon_usage" in data:
            server_side_anonymous_usage = data["server_side_anon_usage"]

        if not isinstance(server_side_anonymous_usage, bool):
            server_side_anonymous_usage = False

        debug = data["debug"] if "debug" in data else False
        if not isinstance(debug, bool):
            debug = False

        campaigns = {
            k: Campaign.from_dict(v)
            for (k, v) in data["campaigns"].items()
        }

        return Configuration(server,
                             site_id,
                             campaigns,
                             timeout,
                             max_retry_delay,
                             max_age_of_old_data,
                             persist_every_n_times,
                             server_side_anonymous_usage,
                             debug)
