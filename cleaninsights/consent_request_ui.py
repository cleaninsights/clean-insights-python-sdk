from typing import Callable, Optional

from cleaninsights.campaign import Campaign
from cleaninsights.consents import CampaignConsent, Feature, FeatureConsent


class ConsentRequestUi:
    """
    Implement this to provide a UI which explains your campaigns.

    Questions you should answer to the user:

    - Why do you need these measurements?
    - How long will they get collected?
    - What might attackers learn from these numbers?
    - How long will you store them on your servers?
    - What are you going to do with these measurements?
    - How will the user benefit from these measurements?
    """

    CompleteFeature = Callable[[bool], None]
    CompleteCampaign = Callable[[bool, Optional[int]], None]
    CompletedFeature = Callable[[Optional[FeatureConsent]], None]
    CompletedCampaign = Callable[[Optional[CampaignConsent]], None]

    def show_feature(self, feature: Feature, complete: CompleteFeature):
        """
        Will be called if it is necessary to ask the user for consent to record a common feature while measuring a campaign.

        :param feature: The feature to record. (e.g. user agent, locale)
        :param complete: The callback which will store the consent or the denial of it.
        """
        raise NotImplementedError

    def show_campaign(self, campaign_id: str, campaign: Campaign, complete: CompleteCampaign):
        """
        Will be called if it is necessary to ask the user for consent to a measurement campaign.

        :param campaign_id: The campaign identifier.
        :param campaign: The campaign configuration.
        :param complete: The callback which will store the consent or the denial of it.
        """
        raise NotImplementedError
