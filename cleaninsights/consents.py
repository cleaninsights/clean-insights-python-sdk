from datetime import datetime, timedelta, timezone
from enum import Enum
from typing import Dict, Optional, Union

from cleaninsights import Campaign


class Feature(Enum):
    LANG = "lang"

    UA = "ua"


class ConsentState(Enum):
    UNCONFIGURED = "unconfigured"
    """
    A campaign with that ID doesn't exist or already expired.
    """

    UNKNOWN = "unknown"
    """
    There's no record of consent. User was probably never asked.
    """

    DENIED = "denied"
    """
    User denied consent. Don't ask again!
    """

    NOT_STARTED = "not_started"
    """
    Consent was given, but consent period has not yet started.
    """

    EXPIRED = "expired"
    """
    Consent was given, but consent period is over. You might ask again for a new period.
    """

    GRANTED = "granted"
    """
    Consent was given and is currently valid.
    """


class Consent:

    def __init__(self,
                 granted: Union[bool, Dict[str, Union[bool, str, int]]],
                 start: Optional[datetime] = None,
                 end: Optional[datetime] = None):

        if start is None:
            start = datetime.now(timezone.utc)
        if end is None:
            end = datetime.now(timezone.utc)

        if isinstance(granted, Dict):
            if "granted" in granted and granted["granted"]:
                self.granted = True
            else:
                self.granted = False

            if "start" in granted and isinstance(granted["start"], (int, float)):
                self.start = datetime.fromtimestamp(float(granted["start"]), timezone.utc)
            else:
                self.start = start

            if "end" in granted and isinstance(granted["end"], (int, float)):
                self.end = datetime.fromtimestamp(float(granted["end"]), timezone.utc)
            else:
                self.end = end

        else:
            self.granted = granted
            self.start = start
            self.end = end

    def state(self) -> ConsentState:
        return ConsentState.GRANTED if self.granted else ConsentState.DENIED


class FeatureConsent(Consent):

    def __init__(self, feature: Feature, consent: Consent):
        super().__init__(consent.granted, consent.start, consent.end)

        self.feature = feature


class CampaignConsent(Consent):

    def __init__(self, campaign_id: str, consent: Consent):
        super().__init__(consent.granted, consent.start, consent.end)

        self.campaign_id = campaign_id

    def state(self) -> ConsentState:
        if not self.granted:
            return ConsentState.DENIED

        now = datetime.now(self.start.tzinfo)

        if now < self.start:
            return ConsentState.NOT_STARTED

        if now > self.end:
            return ConsentState.EXPIRED

        return ConsentState.GRANTED


class Consents:
    """
    This class keeps track of all granted or denied consents of a user.

    There are two different types of consents:
    - Consents for common features like if we're allowed to evaluate the locale or a user agent.
    - Consents per measurement campaign.

    The time of the consent is recorded along with its state: If it was actually granted or denied.

    Consents for common features are given indefinitely, since they are only ever recorded along
    with running campaigns.

    Consents for campaigns only last for a certain amount of days.
    """

    def __init__(self,
                 features: Union[None, Dict[str, Dict[str, Union[bool, str, int]]]] = None,
                 campaigns: Union[None, Dict[str, Dict[str, Union[bool, str, int]]]] = None):

        self.features: Dict[str, Consent] = {}
        self.campaigns: Dict[str, Consent] = {}

        if isinstance(features, Dict):
            for key in features.keys():
                self.features[key] = Consent(features[key])

        if isinstance(campaigns, Dict):
            for key in campaigns.keys():
                self.campaigns[key] = Consent(campaigns[key])

    def grant_feature(self, feature: Feature) -> FeatureConsent:
        """User consents to evaluate a `Feature`."""
        # Don't overwrite original grant timestamp.
        if feature.value not in self.features or not self.features[feature.value].granted:
            self.features[feature.value] = Consent(True)

        return FeatureConsent(feature, self.features[feature.value])

    def deny_feature(self, feature: Feature) -> FeatureConsent:
        """User denies consent to evaluate a `Feature`."""
        # Don't overwrite original deny timestamp.
        if feature.value not in self.features or self.features[feature.value].granted:
            self.features[feature.value] = Consent(False)

        return FeatureConsent(feature, self.features[feature.value])

    def consent_for_feature(self, feature: Feature) -> Optional[FeatureConsent]:
        """
        Returns the consent for a given feature, if any available.

        :param feature: The feature to get the consent for.
        :return: The `FeatureConsent` for the given feature or `None`, if consent unknown.
        """
        if feature.value not in self.features:
            return None

        return FeatureConsent(feature, self.features[feature.value])

    def state_of_feature(self, feature: Feature) -> ConsentState:
        """
        Checks the consent state of a feature.

        :param feature: The feature to check the consent state of.
        :return: The current state of consent.
        """
        consent = self.consent_for_feature(feature)

        return ConsentState.UNKNOWN if consent is None else consent.state()

    def grant_campaign(self, campaign_id: str, campaign: Campaign, length: Optional[int] = None) -> CampaignConsent:
        """
        User consents to run a specific campaign.

        :param campaign_id: The campaign ID.

        :param campaign: The campaign.

        :param length: The customized length of the measurement period, no longer than the maximum period.
        """
        period = campaign.next_total_measurement_period()

        if period:
            end: datetime

            # If a length is provided, use that, but no longer than the maximum period.
            if length:
                end = min(period.end, period.start + timedelta(days=length))
            else:
                end = period.end

            # Always overwrite, since this might be a refreshed consent for a new period.
            self.campaigns[campaign_id] = Consent(True, period.start, end)
        else:
            # Consent is technically granted, but has no effect, as start and end
            # will be set the same.
            self.campaigns[campaign_id] = Consent(True)

        return CampaignConsent(campaign_id, self.campaigns[campaign_id])

    def deny_campaign(self, campaign_id: str) -> CampaignConsent:
        """
        User denies consent to run a specific campaign.

        :param campaign_id: The campaign ID.
        """
        # Don't overwrite original deny timestamp.
        if campaign_id not in self.campaigns or self.campaigns[campaign_id].granted:
            self.campaigns[campaign_id] = Consent(False)

        return CampaignConsent(campaign_id, self.campaigns[campaign_id])

    def consent_for_campaign(self, campaign_id: str) -> Optional[CampaignConsent]:
        """
        Returns the consent for a given campaign, if any available.

        :param campaign_id: The campaign ID to get the consent for.
        :return: The `CampaignConsent` for the given campaign or `None` if consent unknown.
        """
        if campaign_id not in self.campaigns:
            return None

        return CampaignConsent(campaign_id, self.campaigns[campaign_id])

    def state_of_campaign(self, campaign_id: str) -> ConsentState:
        """
        Checks the consent state of a campaign.

        :param campaign_id: The campaign ID to check the consent state of.
        :return: The current state of consent.
        """
        consent = self.consent_for_campaign(campaign_id)

        return ConsentState.UNKNOWN if consent is None else consent.state()
