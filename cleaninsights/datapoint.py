from datetime import datetime, timezone
from typing import Optional


class DataPoint:

    def __init__(self, campaign_id: str, times: int = 1, first: Optional[datetime] = None,
                 last: Optional[datetime] = None):
        """
        :param campaign_id: The campaign ID this data point is for.

        :param times: Number of times this data point has arisen between `first` and `last`.

        :param first: The first time this data point has arisen.

        :param last: The last time this data point has arisen.
        """
        self.campaign_id = campaign_id
        self.times = times
        self.first = first if first else datetime.now(timezone.utc)
        self.last = last if last else datetime.now(timezone.utc)

    def __eq__(self, other):
        return (self.campaign_id == other.campaign_id
                and self.times == other.times
                and self.first == other.first
                and self.last == other.last)
