from datetime import datetime
from typing import Optional
from typing import Union

from cleaninsights.datapoint import DataPoint


class Event(DataPoint):

    def __init__(self, category: str, action: str, name: Optional[str], value: Optional[Union[int, float]],
                 campaign_id: str, times: int = 1, first: Optional[datetime] = None,
                 last: Optional[datetime] = None):
        """
        :param category: The event category. Must not be empty. (e.g. Videos, Music, Games...)

        :param action: The event action. Must not be empty.
            (e.g. Play, Pause, Duration, Add Playlist, Downloaded, Clicked...)

        :param name: The event name.

        :param value: The event value.

        :param campaign_id: The campaign ID this data point is for.

        :param times: Number of times this data point has arisen between `first` and `last`.

        :param first: The first time this data point has arisen.

        :param last: The last time this data point has arisen.
        """
        super().__init__(campaign_id, times, first, last)
        self.category = category
        self.action = action
        self.name = name
        self.value = value

    def __repr__(self):
        return (f"<Visit category={self.category} "
                f"action={self.action} "
                f"name={self.name} "
                f"value={self.value} "
                f"campaign_id={self.campaign_id} "
                f"times={self.times} "
                f"first={self.first} "
                f"last={self.last}>")

    def __eq__(self, other):
        return (self.category == other.category
                and self.action == other.action
                and self.name == other.name
                and self.value == other.value
                and super().__eq__(other))
