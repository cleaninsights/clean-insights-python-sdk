import locale
import platform
from datetime import datetime, timedelta
from typing import List, Union

from cleaninsights.conf import Configuration
from cleaninsights.consents import ConsentState, Feature
from cleaninsights.event import Event
from cleaninsights.store import Store
from cleaninsights.visit import Visit


class Insights:

    def __init__(self, conf: Configuration, store: Store):
        """
        Create an `Insights` object according to configuration with all data from the store which
        is due for offloading to the server.

        :param conf: The current configuration.
        :param store: The current measurement and consents store.
        """
        self.idsite = conf.site_id  # Matomo site ID.
        self.lang: Union[str, None] = None  # Preferred user languages as an HTTP Accept header.
        self.ua: Union[str, None] = None  # User Agent string.
        self.visits: List[Visit] = []  # `Visit` data points.
        self.events: List[Event] = []  # `Event` data points.

        if not conf.server_side_anonymous_usage and store.consents.state_of_feature(
                Feature.LANG) == ConsentState.GRANTED:
            self.lang = Insights.accept_languages()

        if not conf.server_side_anonymous_usage and store.consents.state_of_feature(Feature.UA) == ConsentState.GRANTED:
            self.ua = Insights.user_agent()

        Insights._purge(conf, store)

        for visit in store.visits:
            if visit.campaign_id not in conf.campaigns:
                continue

            now = datetime.now(visit.last.tzinfo)

            # Only send, after aggregation period is over. `last` should contain that date!
            if now > visit.last:
                self.visits.append(visit)

        for event in store.events:
            if event.campaign_id not in conf.campaigns:
                continue

            now = datetime.now(event.last.tzinfo)

            # Only send, after aggregation period is over. `last` should contain that date!
            if now > event.last:
                self.events.append(event)

    @property
    def is_empty(self):
        return len(self.visits) < 1 and len(self.events) < 1

    def clean(self, store: Store):
        """
        Removes all visits and events from the given `Store`, which are also available in here.

        This should be called, when all `Insights` were offloaded at the server successfully.

        :param store: The store where the `Visit`s and `Event`s in here came from.
        """
        for visit in self.visits:
            store.visits[:] = [v for v in store.visits if v != visit]

        for event in self.events:
            store.events[:] = [e for e in store.events if e != event]

    @classmethod
    def accept_languages(cls) -> str:
        """Create an HTTP-accept-language-header-like string from the information we can get from our environment."""
        loc = locale.getdefaultlocale()

        if len(loc) > 0 and loc[0] is not None:
            return f'{loc[0]};q=1.0'
        else:
            return ''

    @classmethod
    def user_agent(cls) -> str:
        return platform.platform(True, False)

    @classmethod
    def _purge(cls, conf: Configuration, store: Store):
        """
        Removes `DataPoint`s, which are too old. These were never been sent, otherwise, they would have
        been removed, already.

        Remove them now, if they're over the threshold, to not accumulate too many `DataPoints` and
        therefore reduce privacy.
        """
        if len(store.visits) > 0:
            tzinfo = store.visits[0].first.tzinfo
        elif len(store.events) > 0:
            tzinfo = store.events[0].first.tzinfo
        else:
            return

        threshold = datetime.now(tzinfo) - timedelta(days=conf.max_age_of_old_data)

        store.visits[:] = [v for v in store.visits if v.last >= threshold]

        store.events[:] = [e for e in store.events if e.last >= threshold]
