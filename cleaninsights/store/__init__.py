from typing import Callable, Dict, List, Optional, Union

from cleaninsights.consents import Consents
from cleaninsights.datapoint import DataPoint
from cleaninsights.event import Event
from cleaninsights.visit import Visit


class Store:
    """
    The store holds the user's consents to the different `Feature`s and `Campaign`s,
    and their `Visit` and `Event` measurements.

    If you want to implement your own persistence of the store (e.g. because you
    want to write it in a database instead of the file system) and your own
    implementation of the transmission to the Matomo/CIMP backend (e.g. because
    you want to tunnel the requests through a proxy or add your own encryption layer),
    then create a subclass of this class and implement the `#__init__`,
    `#persist` and `#send` methods.
    """

    def __init__(self, debug: Optional[Callable[[str], None]] = None, **kwargs):
        """

        :param debug: Optional function to output debug messages.
        :param kwargs: Optional arguments your implementation might need for loading the store.
        """
        self.consents = Consents()
        self.visits: List[Visit] = []
        self.events: List[Event] = []

        if debug:
            debug(f"Store created from the following arguments: {kwargs}")
            kwargs["debug"] = debug

        data = self.load(**kwargs)
        if data is None:
            if debug:
                debug("Storage doesn't exist or isn't readable.")

            return

        if "consents" in data and isinstance(data["consents"], Consents):
            self.consents = data["consents"]

        if "visits" in data and isinstance(data["visits"], List):
            self.visits = [v for v in data["visits"] if isinstance(v, Visit)]

        if "events" in data and isinstance(data["events"], List):
            self.events = [e for e in data["events"] if isinstance(e, Event)]

        if debug:
            debug("Data loaded from storage.")

    def load(self, **kwargs) -> Optional[Dict[str, Union[Consents, List[DataPoint]]]]:
        """
        This method gets called from the constructor and will receive the same parameters, as the constructor got.

        :param args: Arguments your implementation might need for loading the store.
        :param kwargs: Arguments your implementation might need for loading the store.
        :return: Deserialized store data.
        """
        raise NotImplementedError

    def persist(self, asynchronous: bool, done: Callable[[Optional[Exception]], None]):
        """
        This method gets called, when the SDK is of the opinion, that persisting the `Store` is in order.

        This is partly controlled by the `Configuration.persist_every_n_times` configuration option, and by calls to
        `CleanInsights#persist`.

        If possible, try to honor the `async` flag:
        If it's true, it is set so, because the SDK wants to reduce impact on user responsivity as much as possible.
        If it's false, the SDK wants to make sure, that the call finishes before the app gets killed.

        :param asynchronous: Indicates, if the persistence should be done asynchronously or synchronously.
            E.g. a persist call during the exit of an app should be done synchronously,
            otherwise the operation might never get finished because the OS kills the server too early.

        :param done: Callback, when the operation is finished, either successfully or not.
            If no error is returned, the operation is considered successful and the internal counter will be set back again.
        """
        raise NotImplementedError

    def send(self, data: str, server: str, timeout: int, done: Callable[[Optional[Exception]], None]):
        """
        This method gets called, when `CleanInsights` gathered enough data for a
        time period and is ready to send the data to a CIMP (CleanInsights Matomo Proxy).

        :param data: The serialized JSON for a POST request to a CIMP.

        :param server: The server URL from `Configuration.server`.

        :param timeout: The timeout in seconds from `Configuration.timeout`.

        :param done: Callback, when the operation is finished, either successfully or not.
            If no error is returned, the data sent will be removed from the
            store and the store persisted.
        """
        raise NotImplementedError
