import http.client
import json
import urllib.parse
from pathlib import Path
from threading import Thread
from typing import Callable, Dict, List, Optional, Union

from cleaninsights import DataPoint, Store
from cleaninsights.ci_json import CleanInsightsDecoder, CleanInsightsEncoder
from cleaninsights.consents import Consents


class DefaultStore(Store):

    __storage_filename = "cleaninsights.json"

    def __init__(self, debug: Optional[Callable[[str], None]] = None, **kwargs):
        self.__storage_file: Optional[Path] = None

        if "storage_file" in kwargs and isinstance(kwargs["storage_file"], Path):
            self.__storage_file = kwargs["storage_file"]

        elif "storage_dir" in kwargs and isinstance(kwargs["storage_dir"], Path):
            self.__storage_file = kwargs["storage_dir"].joinpath(self.__storage_filename)

        super().__init__(debug, **kwargs)

    def load(self, **kwargs) -> Optional[Dict[str, Union[Consents, List[DataPoint]]]]:
        if "debug" in kwargs and callable(kwargs["debug"]):
            debug = kwargs["debug"]

            debug(f"Try to load store from \"{self.__storage_file}\".")
        else:
            debug = None

        if self.__storage_file is None:
            return None

        try:
            with open(self.__storage_file, "r") as fp:
                data = json.load(fp, cls=CleanInsightsDecoder)
        except Exception as error:
            data = None
            if debug:
                debug(error.__str__())

        return data

    def persist(self, asynchronous: bool, done: Callable[[Optional[Exception]], None]):
        if self.__storage_file is None:
            done(IOError("No storage file available. Could not persist!"))
            return

        def closure():
            with open(self.__storage_file, "w") as fp:
                try:
                    json.dump(self, fp, cls=CleanInsightsEncoder)
                except Exception as error:
                    done(error)
                    return

            done(None)

        if asynchronous:
            Thread(target=closure).start()
        else:
            closure()

    def send(self, data: str, server: str, timeout: int, done: Callable[[Optional[Exception]], None]):
        def closure():
            url = urllib.parse.urlparse(server)
            conn = http.client.HTTPSConnection(url.netloc, timeout=timeout)
            headers = {"Content-Type": "application/json"}

            try:
                conn.request("POST", url.path, data.encode("utf-8"), headers)
                resp = conn.getresponse()

                if resp.status not in ['200', '204']:
                    done(Exception(f'HTTP Error {resp.status}: {resp.reason}'))
                else:
                    done(None)

            except Exception as error:
                done(error)

        Thread(target=closure).start()
