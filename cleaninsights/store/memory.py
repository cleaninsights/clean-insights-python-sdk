import http.client
import urllib.parse
from typing import Callable, Dict, List, Optional, Union

from cleaninsights import DataPoint
from cleaninsights.consents import Consents
from cleaninsights.store import Store


class MemoryStore(Store):
    """A memory-resident store with no persistence."""

    def load(self, **kwargs) -> Optional[Dict[str, Union[Consents, List[DataPoint]]]]:
        """This class provides no persistence and so this function returns no data."""
        return None

    def persist(self, asynchronous: bool, done: Callable[[Optional[Exception]], None]):
        """This class provides no persistence and so this function does nothing."""
        done(None)

    def send(self, data: str, server: str, timeout: int, done: Callable[[Optional[Exception]], None]):
        url = urllib.parse.urlparse(server)
        conn = http.client.HTTPSConnection(url.netloc, timeout=timeout)
        headers = {"Content-Type": "application/json"}

        try:
            conn.request("POST", url.path, data.encode("utf-8"), headers)
            resp = conn.getresponse()

            if resp.status not in ['200', '204']:
                done(Exception(f'HTTP Error {resp.status}: {resp.reason}'))
            else:
                done(None)

        except Exception as error:
            done(error)
