from datetime import datetime
from typing import List, Optional

from cleaninsights.datapoint import DataPoint


class Visit(DataPoint):

    def __init__(self, scene_path: List[str], campaign_id: str, times: int = 1,
                 first: Optional[datetime] = None, last: Optional[datetime] = None):
        """
        :param scene_path: A hierarchical path to the scene visited.

        :param campaign_id: The campaign ID this data point is for.

        :param times: Number of times this data point has arisen between `first` and `last`.

        :param first: The first time this data point has arisen.

        :param last: The last time this data point has arisen.
        """
        super().__init__(campaign_id, times, first, last)
        self.scene_path = scene_path

    def __repr__(self):
        return (f"<Visit scene_path={self.scene_path} "
                f"campaign_id={self.campaign_id} "
                f"times={self.times} "
                f"first={self.first} "
                f"last={self.last}>")

    def __eq__(self, other):
        return self.scene_path == other.scene_path and super().__eq__(other)
