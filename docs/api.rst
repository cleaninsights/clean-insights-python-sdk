API Documentation
=================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   aggregation_rule
   aggregation_period
   campaign
   cleaninsights
   conf
   consents
   datapoint
   event
   visit
   store
