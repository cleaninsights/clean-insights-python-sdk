import json
import tempfile
import time
import unittest
from datetime import timezone, datetime, timedelta
from pathlib import Path
from typing import Dict, List, Union, Callable, Any, Optional

from cleaninsights import CleanInsights, ConsentRequestUi
from cleaninsights.aggregation_rule import EventAggregationRule
from cleaninsights.campaign import Campaign
from cleaninsights.conf import Configuration
from cleaninsights.consents import ConsentState, Feature, Consent, CampaignConsent
from cleaninsights.event import Event
from cleaninsights.insights import Insights
from cleaninsights.ci_json import CleanInsightsEncoder
from cleaninsights.store import Store
from cleaninsights.store.default import DefaultStore
from cleaninsights.store.memory import MemoryStore
from cleaninsights.visit import Visit


class Test(unittest.TestCase, ConsentRequestUi):
    confJson = """{
        "server": "http://localhost:8080/ci/cleaninsights.php",
        "siteId": 1,
        "timeout": 1,
        "maxRetryDelay": 1,
        "maxAgeOfOldData": 1,
        "persistEveryNTimes": 1,
        "serverSideAnonymousUsage": false,
        "debug": true,
        "campaigns": {
            "test": {
                "start": "2021-01-01T00:00:00-00:00",
                "end": "2099-12-31T23:59:59-00:00",
                "aggregationPeriodLength": 1,
                "numberOfPeriods": 90,
                "onlyRecordOnce": false,
                "eventAggregationRule": "avg",
                "strengthenAnonymity": false
            }
        }
    }"""

    conf = Configuration("http://localhost:8080/ci/cleaninsights.php",
                         1,
                         {"test": Campaign(datetime(2021, 1, 1, tzinfo=timezone.utc),
                                           datetime(2099, 12, 31, 23, 59, 59, tzinfo=timezone.utc),
                                           1,
                                           90,
                                           False,
                                           EventAggregationRule.AVG,
                                           False)},
                         1,
                         1,
                         1,
                         1,
                         False,
                         True)

    ci: CleanInsights

    def setUp(self):
        store = MemoryStore()

        self.ci = CleanInsights(self.confJson, store)

    def test_conf(self):
        self.assertEqual(self.conf, self.ci.conf)

    def test_deny_consent(self):
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.LANG))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.UA))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_campaign("test"))

        self.ci.deny_feature(Feature.LANG)
        self.ci.deny_feature(Feature.UA)
        self.ci.deny_campaign("test")

        self.assertEqual(2, len(self.ci.feature_consents))
        self.assertEqual(1, len(self.ci.campaign_consents))

        consent = self.ci.feature_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertFalse(consent.granted)
        self.assertEqual(ConsentState.DENIED, self.ci.state_of_feature(Feature.LANG))

        consent = self.ci.feature_consent_by_index(1)
        self.assertIsNotNone(consent)
        self.assertFalse(consent.granted)
        self.assertEqual(ConsentState.DENIED, self.ci.state_of_feature(Feature.UA))

        consent = self.ci.feature_consent_by_index(2)
        self.assertIsNone(consent)

        consent = self.ci.campaign_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertFalse(consent.granted)

        consent = self.ci.campaign_consent_by_index(1)
        self.assertIsNone(consent)

        self.assertFalse(self.ci.is_campaign_currently_granted("test"))
        self.assertEqual(ConsentState.DENIED, self.ci.state_of_campaign("test"))

    def test_grant_consent(self):
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.LANG))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.UA))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_campaign("test"))

        self.ci.grant_feature(Feature.LANG)
        self.ci.grant_feature(Feature.UA)
        self.ci.grant_campaign('test')

        self.assertEqual(2, len(self.ci.feature_consents))
        self.assertEqual(1, len(self.ci.campaign_consents))

        consent = self.ci.feature_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.granted)
        self.assertEqual(ConsentState.GRANTED, self.ci.state_of_feature(Feature.LANG))

        consent = self.ci.feature_consent_by_index(1)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.granted)
        self.assertEqual(ConsentState.GRANTED, self.ci.state_of_feature(Feature.UA))

        consent = self.ci.feature_consent_by_index(2)
        self.assertIsNone(consent)

        midnight_today = (datetime.now(self.ci.conf.campaigns["test"].start.tzinfo)
                          .replace(hour=0, minute=0, second=0, microsecond=0))

        consent = self.ci.campaign_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.start >= midnight_today)
        self.assertTrue(consent.granted)

        consent = self.ci.campaign_consent_by_index(1)
        self.assertIsNone(consent)

        self.assertTrue(self.ci.is_campaign_currently_granted('test'))
        self.assertEqual(ConsentState.GRANTED, self.ci.state_of_campaign("test"))

    def test_grant_consent_strengthened_anonymity(self):
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.LANG))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_feature(Feature.UA))
        self.assertEqual(ConsentState.UNKNOWN, self.ci.state_of_campaign("test"))

        ci = self.__get_strengthened_ci()

        ci.grant_feature(Feature.LANG)
        ci.grant_feature(Feature.UA)
        ci.grant_campaign('test')

        self.assertEqual(2, len(ci.feature_consents))
        self.assertEqual(1, len(ci.campaign_consents))

        consent = ci.feature_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.granted)
        self.assertEqual(ConsentState.GRANTED, ci.state_of_feature(Feature.LANG))

        consent = ci.feature_consent_by_index(1)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.granted)
        self.assertEqual(ConsentState.GRANTED, ci.state_of_feature(Feature.UA))

        consent = ci.feature_consent_by_index(2)
        self.assertIsNone(consent)

        midnight_tomorrow = ((datetime.now(ci.conf.campaigns["test"].start.tzinfo)
                              .replace(hour=0, minute=0, second=0, microsecond=0))
                             + timedelta(days=1))

        consent = ci.campaign_consent_by_index(0)
        self.assertIsNotNone(consent)
        self.assertTrue(consent.start >= midnight_tomorrow)
        self.assertTrue(consent.granted)

        consent = ci.campaign_consent_by_index(1)
        self.assertIsNone(consent)

        self.assertFalse(ci.is_campaign_currently_granted('test'))
        self.assertEqual(ConsentState.NOT_STARTED, ci.state_of_campaign('test'))

    def test_invalid_campaign(self):
        self.assertIsNone(self.ci.consent_for_campaign("foobar"))

        self.assertEqual(ConsentState.UNCONFIGURED, self.ci.state_of_campaign("foobar"))

        def completed(granted: bool):
            self.assertFalse(granted)

        self.ci.request_consent_for_campaign("foobar", self, completed)

        self.assertIsNone(self.ci.grant_campaign("foobar"))

        self.assertIsNone(self.ci.deny_campaign("foobar"))

        self.assertEqual(0, len(self.ci.campaign_consents))

        self.assertFalse(self.ci.is_campaign_currently_granted("foobar"))

    def test_persistence(self):
        self.ci.grant_feature(Feature.LANG)
        self.ci.grant_feature(Feature.UA)
        self.ci.grant_campaign("test")

        self.assertTrue(self.ci.is_campaign_currently_granted("test"))

        self.ci.measure_visit(['foo'], 'test')
        self.ci.measure_event('foo', 'bar', 'test', 'baz', 4567)

        self.ci.persist()

        store = self.ci._store

        midnight = datetime.now(timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
        midnight_tomorrow = (datetime.now(timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
                             + timedelta(days=1))

        self.assertEqual([Visit(['foo'], 'test', 1, midnight, midnight_tomorrow)], store.visits)
        self.assertEqual([Event('foo', 'bar', 'baz', 4567, 'test', 1,
                                midnight, midnight_tomorrow)], store.events)

    def test_persistence_strengthened_anonymity(self):
        ci = self.__get_strengthened_ci()

        ci.grant_feature(Feature.LANG)
        ci.grant_feature(Feature.UA)
        ci.grant_campaign("test")

        ci.measure_visit(['foo'], 'test')
        ci.measure_event('foo', 'bar', 'test', 'baz', 4567)

        store = ci._store

        self.assertEqual([], store.visits)  # Consent will only start tomorrow!
        self.assertEqual([], store.events)  # Consent will only start tomorrow!

        # Re-init with faked store.
        ci = CleanInsights(self.confJson, Test.__fake_yesterday_consent())

        self.assertTrue(ci.is_campaign_currently_granted('test'))

        ci.measure_visit(['foo'], 'test')
        ci.measure_event('foo', 'bar', 'test', 'baz', 4567)

        ci.persist()

        store = ci._store

        midnight = datetime.now(timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
        midnight_tomorrow = (datetime.now(timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)
                             + timedelta(days=1))

        self.assertEqual([Visit(['foo'], 'test', 1, midnight, midnight_tomorrow)], store.visits)
        self.assertEqual([Event('foo', 'bar', 'baz', 4567, 'test', 1,
                                midnight, midnight_tomorrow)], store.events)

    def test_purge(self):
        store = TestStore()

        day_before_yesterday = datetime.now() - timedelta(days=2)

        store.visits.append(Visit(['foo'], "x", 1, day_before_yesterday, day_before_yesterday))
        store.events.append(Event("foo", "bar", None, None, "x", 1,
                                  day_before_yesterday, day_before_yesterday))

        TestInsights.purge(self.conf, store)

        self.assertEqual(0, len(store.visits))
        self.assertEqual(0, len(store.events))

        now = datetime.now()

        store.visits.append(Visit(['foo'], "x", 1, now, now))
        store.events.append(Event("foo", "bar", None, None, "x", 1, now, now))

        TestInsights.purge(self.conf, store)

        self.assertEqual(1, len(store.visits))
        self.assertEqual(1, len(store.events))

    def test_serialize_insights(self):
        insights = Insights(self.conf, TestStore())

        self.assertEqual("{\"idsite\": 1, \"lang\": null, \"ua\": null, \"visits\": [], \"events\": []}",
                         json.dumps(insights, cls=CleanInsightsEncoder))

        store = TestStore()

        first_jan_2022 = datetime(2022, 1, 1, 0, 0, 0, 0, tzinfo=timezone.utc)
        second_jan_2022 = datetime(2022, 1, 2, 0, 0, 0, 0, tzinfo=timezone.utc)

        store.visits.append(Visit(["foo", "bar"], "test", 1, first_jan_2022, second_jan_2022))

        store.events.append(Event("foo", "bar", "baz", 6.66, "test", 1,
                                  first_jan_2022, second_jan_2022))

        c = Configuration(self.conf.server, self.conf.site_id, self.conf.campaigns, self.conf.timeout,
                          self.conf.max_retry_delay, 30000, self.conf.persist_every_n_times,
                          self.conf.server_side_anonymous_usage, self.conf.debug)

        insights = Insights(c, store)

        self.assertEqual(
            "{\"idsite\": 1, \"lang\": null, \"ua\": null, \"visits\": [{\"action_name\": \"foo/bar\", \"campaign_id\": \"test\", \"times\": 1, \"period_start\": 1640995200, \"period_end\": 1641081600}], \"events\": [{\"category\": \"foo\", \"action\": \"bar\", \"name\": \"baz\", \"value\": 6.66, \"campaign_id\": \"test\", \"times\": 1, \"period_start\": 1640995200, \"period_end\": 1641081600}]}",
            json.dumps(insights, cls=CleanInsightsEncoder))

    def test_server_test(self):
        ci = CleanInsights(self.confJson, TestStore())

        ci.test_server(lambda error: self.assertIsNone(error))

    def test_invalid_configs(self):
        self.__test_config_check(
            Configuration("", -1, {}, 0, 0, 0,
                          0, False, False),
            False)

        self.__test_config_check(
            Configuration("x", 1, {}, 0, 0, 0,
                          0, False, False),
            False)

        self.__test_config_check({"server": "x", "siteId": 1, "campaigns": {}}, False)

        self.__test_config_check(
            {"server": "x", "siteId": 1, "campaigns": {
                "foobar": {"start": "1970-01-01T00:00:00", "end": "1970-01-01T00:00:00",
                           "aggregationPeriodLength": 1}}},
            False)

        self.__test_config_check(
            {"server": "https://example.org/", "siteId": 1, "campaigns": {
                "foobar": {"start": "1970-01-01T00:00:00", "end": "1970-01-01T00:00:00",
                           "aggregationPeriodLength": 1}}},
            True)

    def test_default_store_send(self):
        def debug(msg):
            print(msg)

        store = DefaultStore(debug)

        def done(error: Optional[Exception]):
            self.assertIsNotNone(error)
            self.assertEqual("HTTP Error 404: Not Found", error.__str__())

        store.send("", "http://example.org/test", 2, done)

    def test_auto_track(self):
        temp = tempfile.TemporaryDirectory()
        storage_dir = Path(temp.name)

        storage_file = storage_dir.joinpath("cleaninsights.json")

        storage_file.unlink(missing_ok=True)

        store = DefaultStore(storage_dir=storage_dir)

        yesterday = datetime.now(timezone.utc) - timedelta(days=1)
        in_one_year = datetime.now(timezone.utc) + timedelta(days=365)

        store.consents.campaigns["visits"] = CampaignConsent("visits", Consent(True, yesterday, in_one_year))

        def done(error: Optional[Exception]):
            self.assertIsNone(error)

        store.persist(False, done)

        CleanInsights.auto_track(storage_dir, self.conf.server, self.conf.site_id)

        time.sleep(0.5)  # Otherwise, there can be race condition when reloading the store.

        store = DefaultStore(storage_dir=storage_dir)

        first = datetime.now(timezone.utc).replace(hour=0, minute=0, second=0, microsecond=0)

        last = first + timedelta(days=1)

        self.assertEqual(Visit([], "visits", first=first, last=last), store.visits[0])

    def show_feature(self, feature: Feature, complete: ConsentRequestUi.CompleteFeature):
        # We should not end up here.
        self.assertTrue(False)

    def show_campaign(self, campaign_id: str, campaign: Campaign, complete: ConsentRequestUi.CompleteCampaign):
        # We should not end up here.
        self.assertTrue(False)

    def __get_strengthened_ci(self) -> CleanInsights:
        co = self.conf
        ca = self.conf.campaigns["test"]

        return CleanInsights(Configuration(
            co.server,
            co.site_id,
            {
                "test": Campaign(
                    ca.start,
                    ca.end,
                    ca.aggregation_period_length,
                    ca.number_of_periods,
                    ca.only_record_once,
                    ca.event_aggregation_rule,
                    True)
            },
            co.timeout,
            co.max_retry_delay,
            co.max_age_of_old_data,
            co.persist_every_n_times,
            co.server_side_anonymous_usage,
            co.debug), MemoryStore())

    def __test_config_check(self, conf: Union[Configuration, Dict[str, Any]], is_good: bool):
        if isinstance(conf, Dict):
            conf = Configuration.from_dict(conf)

        count = 0

        def debug(msg):
            nonlocal count
            count += 1

        result = conf.check(debug)

        if is_good:
            self.assertEqual(0, count)
            self.assertTrue(result)
        else:
            self.assertEqual(1, count)
            self.assertFalse(result)

    @staticmethod
    def __fake_yesterday_consent() -> MemoryStore:
        store = MemoryStore()

        yesterday = datetime.now(timezone.utc) - timedelta(days=1)

        end = datetime.now(timezone.utc) + timedelta(days=3)

        store.consents.campaigns['test'] = Consent(True, yesterday, end)

        return store


class TestStore(Store):

    def load(self, **kwargs) -> Optional[Dict[str, Union[Dict[str, Dict[str, Union[bool, str]]], List[Dict[str, Union[str, int, float]]]]]]:
        return None

    def send(self, data: str, server: str, timeout: int, done: Callable[[Optional[Exception]], None]):
        done(Exception('HTTP Error 400: Bad Request'))


if __name__ == '__main__':
    unittest.main()


class TestInsights(Insights):

    @classmethod
    def purge(cls, conf: Configuration, store: Store):
        Insights._purge(conf, store)
